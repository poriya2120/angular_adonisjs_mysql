import{
  Component,
  OnInit,
  Component
} from '@angular/core'

import {Task } from './task'
import {TaskService} from './tasks.service'
import {HttpClient} from '@angular/common/http'
import { throws } from 'assert'

@Component({
  selector:'app-tasks',
  templateUrl:'./tasks.componets.html',
  providers:[TaskService]

})

export class TaskCompoent implements OnInit{
  tasks:Task[]
  editTask:Task
  constructor(private taskservice:TaskService,private http:HttpClient)
  {

  }

  ngOnInit(){
    this.getTasks()
  }

  getTasks():void{
    this.taskservice.getTasks()
    .subscribe( tasks =>(this.tasks=tasks))
  }

  add(task_name:string):void{
    this.editTask=undefined
    task_name=task_name.trim()
    if(!task_name)
    {
      return
    }
    const newTask:Task={task_name} as Task
    this.taskservice.addTask(newTask)
    .subscribe(()=>this.getTasks())
  }

  delete(task:Task):void{
    //have question to a!==task
    this.tasks=this.tasks.filter(a=>a!==task)
    this.taskservice.deleteTask(task.id)
    .subscribe(()=>console.log('task deleted'))
  }

  edit(task){
    this.editTask=task
  }

  update(){
    if(this.editTask){
      this.taskservice.updateTask(this.editTask)
        .subscribe(()=>this.getTasks())
    }
    this.editTask=undefined
  }


}
